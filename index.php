<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Halaman PHP</title>
</head>
<body>
    <?php 
        $firstname = "Erick";
        //var_dump($firstname);
        
        $lastname = "Kurniawan";

        echo "Nama anda : $firstname $lastname <br/>";

        echo "<span style='color:red;'>Hello PHP ! </span></br/>";
        echo "Learn PHP is super easy !<br><br>";

        $bil1 = 12;
        $bil2 = 35.3;

        $hasil = $bil1+$bil2;
        echo "Hasil $bil1 + $bil2 = $hasil";

        
    ?>
</body>
</html>